FROM php:7.2-apache-buster

RUN a2enmod rewrite

RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

RUN apt update && apt install -y zip zlib1g zlib1g-dev libwebp-dev \
    libjpeg62-turbo-dev libpng-dev libxpm-dev libfreetype6-dev \
    libc-client-dev libkrb5-dev

RUN docker-php-ext-configure zip \
    && docker-php-ext-install zip

RUN docker-php-ext-configure gd --with-gd --with-webp-dir --with-jpeg-dir \
    --with-png-dir --with-zlib-dir --with-xpm-dir --with-freetype-dir \
    && docker-php-ext-install gd

RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl  \
    && docker-php-ext-install imap

RUN docker-php-ext-install mysqli

RUN echo "upload_max_filesize=10M" >> /usr/local/etc/php/php.ini

EXPOSE 80